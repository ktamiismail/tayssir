package com.mercuriete.mrz.myapplication;


import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.mercuriete.mrz.myapplication.reader.ApplicationService;
import com.mercuriete.mrz.myapplication.reader.GetSocket;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends Activity {

    private Button login;
    EditText email;
    EditText password;
    CheckBox remember;
    SharedPreferences databaseLogin;
    SharedPreferences.Editor databaseLogineditor;
    private Socket socket;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        email=(EditText)findViewById(R.id.email);
        password=(EditText)findViewById(R.id.password);
        remember=(CheckBox)findViewById(R.id.remember) ;
        databaseLogin=getSharedPreferences("login", getApplicationContext().MODE_PRIVATE);
        databaseLogineditor=databaseLogin.edit();

        GetSocket app = new GetSocket();
        socket = app.getSocket();
        socket.connect();
        socket.on("Authentification",handleIncomingAuthentifica);
        socket.on("nonAutho",handleIncomingRefuseAuthentifica);

        if(!databaseLogin.getString("email","vide").equals("vide") && !databaseLogin.getString("password","vide").equals("vide") && databaseLogin.getBoolean("login",false) )
        {
        LoginActivity.this.moveToHome();
        }
        getIndentification();

        login=(Button)findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!LoginActivity.this.email.getText().toString().isEmpty() && !LoginActivity.this.password.getText().toString().isEmpty()){
                    sendAuthentification();
                }
            }
        });

    }


    public boolean sendAuthentification(){
        JSONObject sendText = new JSONObject();
        try{
            sendText.put("email",LoginActivity.this.email.getText().toString());
            sendText.put("password",LoginActivity.this.password.getText().toString());
            socket.emit("Authentifier", sendText);
            LoginActivity.this.login.setEnabled(false);
            return true;
        }catch(JSONException e){
                return false;
        }
    }


    public void moveToHome(){
        Intent i=new Intent(getApplicationContext(), Home.class);
        startActivity(i);
        finish();
    }


    public void registerLogin(boolean remember){
        databaseLogineditor.putString("email",LoginActivity.this.email.getText().toString());
        databaseLogineditor.putString("password",LoginActivity.this.password.getText().toString());
        databaseLogineditor.putBoolean("login",true);
        if(remember)
            databaseLogineditor.putBoolean("remember",true);
        else
            databaseLogineditor.putBoolean("remember",false);

        databaseLogineditor.commit();
        if(!isMyServiceRunning(ApplicationService.class)) {
            startService(new Intent(getApplicationContext(), ApplicationService.class));
        }
    }


    public void getIndentification(){
        LoginActivity.this.email.setText(databaseLogin.getString("email",""));
        LoginActivity.this.password.setText(databaseLogin.getString("password",""));
    }




    private Emitter.Listener handleIncomingRefuseAuthentifica = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    try {
                        String a=data.getString("msg").toString();
                        Toast.makeText(getApplicationContext(),"error identification",Toast.LENGTH_LONG);

                    } catch (JSONException e) {

                    }

                }
            });
        }
    };

    private Emitter.Listener handleIncomingAuthentifica = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                         registerLogin(LoginActivity.this.remember.isChecked());
                         moveToHome();

                }
            });
        }
    };
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}

