package com.mercuriete.mrz.myapplication.reader;

public class User {
    private int id;
    private String email;
    private String password;


    public int getId() {
        return id;
    }

    public String getPassword() {
        return  password;

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User(int id,String email,String password){
        this.id=id;
        this.email=email;
        this.password=password;
    }


}
