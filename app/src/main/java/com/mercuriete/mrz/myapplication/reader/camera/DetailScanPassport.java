package com.mercuriete.mrz.myapplication.reader.camera;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.github.nkzawa.socketio.client.Socket;
import com.mercuriete.mrz.myapplication.R;
import com.mercuriete.mrz.myapplication.reader.DataBase;
import com.mercuriete.mrz.myapplication.reader.GetSocket;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DetailScanPassport extends Activity {
    String nom,prenom,prenomN,passportNumero,dateExpiration,dateNaissance,nationality,sexe,email,cni;
    EditText nomT,prenomT,docnT,dateET,dateNT,nationalityT,cniT;
    RadioGroup sexegroup;
    RadioButton femme,homme;
    Button valider;
    private Socket socket;
    DatePickerDialog d;
    Calendar c=Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        GetSocket app = new GetSocket();
        socket = app.getSocket();
        socket.connect();
        loadLocale();
        setContentView(R.layout.activity_detail_scan_passport);
        setTheme(R.style.Theme_AppCompat);
        super.onCreate(savedInstanceState);
         nomT= (EditText)(findViewById(R.id.nom));
         prenomT= (EditText)(findViewById(R.id.prenom));
         docnT= (EditText)(findViewById(R.id.Npassport));
         dateET= (EditText)(findViewById(R.id.dateExpiration ));
         dateNT= (EditText)(findViewById(R.id.DateNaissance ));
         nationalityT=(EditText)(findViewById(R.id.nationality ));
         cniT=(EditText)(findViewById(R.id.cni));
         sexegroup=(RadioGroup)findViewById(R.id.groupsexe);
         femme=(RadioButton)findViewById(R.id.rbfemme);
        homme=(RadioButton)findViewById(R.id.rbhomme);
        valider=(Button) findViewById(R.id.valider);
        Intent intent=getIntent();
        nom=intent.getStringExtra("nom");
        prenom=intent.getStringExtra("prenom");
        prenomN=prenom.replace('<',' ').trim();
        passportNumero=intent.getStringExtra("numeroPassport");
        dateExpiration=intent.getStringExtra("dateExpiration");
        dateNaissance=intent.getStringExtra("dateNaissance");
        nationality=intent.getStringExtra("nationality");
        cni=intent.getStringExtra("cni");
        sexe=intent.getStringExtra("sexe") ;
        chargeEdittexts();

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              sendScan();
            }



        });

        dateNT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d=new DatePickerDialog(DetailScanPassport.this, R.style.dateTimepicker,new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        dateNT.setText(day+"-"+month+"-"+year);
                    }
                },Integer.parseInt(dateNT.getText().toString().split("-")[2]),Integer.parseInt(dateNT.getText().toString().split("-")[1]),Integer.parseInt(dateNT.getText().toString().split("-")[0]));
                d.show();
            }
        });




        dateET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d=new DatePickerDialog(DetailScanPassport.this, R.style.dateTimepicker,new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        dateET.setText(day+"-"+month+"-"+year);
                    }
                },Integer.parseInt(dateET.getText().toString().split("-")[2]),Integer.parseInt(dateET.getText().toString().split("-")[1]),Integer.parseInt(dateET.getText().toString().split("-")[0]));
                d.show();
            }
        });
    }

    public String FilterDate(String date){
        String n=date.replace('Z','2').replace('O','0');
        return n;

    }

    public String formaterDate(String d) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yymmdd");
        SimpleDateFormat format2 = new SimpleDateFormat("dd-mm-yyyy");
        FilterDate(d);
        Date   date= format.parse(d);
        String dateN=format2.format(date);
        System.err.println("Date++++++++++++"+dateN);
        return dateN;
    }


    public void chargeEdittexts(){
        nomT.setText(nom);
        prenomT.setText(prenomN);
        docnT.setText(passportNumero);
        cniT.setText(cni);

        try {
            dateNT.setText( formaterDate(dateNaissance));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            dateET.setText(formaterDate(dateExpiration));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        nationalityT.setText(nationality);
        if(sexe.contains("FE")){
            femme.setChecked(true);
        }
        else
            homme.setChecked(true);
    }


    private void setLocale(String lang){
        System.err.println(lang );
        Locale locale=new Locale(lang);
        Locale.setDefault(locale);
        Configuration configuration=new Configuration();
        configuration.locale=locale;
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
        editor.putString("My_Lang",lang);
        editor.apply();
    }


    public void loadLocale(){
        SharedPreferences prefs=getSharedPreferences("Settings",Activity.MODE_PRIVATE);
        String language=prefs.getString("My_Lang","");
        setLocale(language);
    }

    private void sendScan(){
        JSONObject sendText = new JSONObject();
        try{
            sendText.put("prenom",prenomT.getText());
            sendText.put("nom",nomT.getText());
            sendText.put("dateNaissance",dateNT.getText());
            sendText.put("dateExpiration",dateET.getText());
            sendText.put("nationality",dateET.getText());
            sendText.put("numeroPassport",docnT.getText());
            sendText.put("sexe",sexe);
            sendText.put("cni",cni);
            sendText.put("email",getSharedPreferences("login", getBaseContext().MODE_PRIVATE).getString("email","email"));

            socket.emit("scan", sendText);
        }catch(JSONException e){
        }
    }

}
