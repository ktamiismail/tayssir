package com.mercuriete.mrz.myapplication;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.annotation.NonNull;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.mercuriete.mrz.myapplication.reader.ApplicationService;
import com.mercuriete.mrz.myapplication.reader.CaptureActivity;

import java.util.Locale;

public class Home extends Activity {
    Intent i ;
    SharedPreferences databaseLogin;
    SharedPreferences.Editor databaseLogineditor;
    LinearLayout scannerLinear,connexionLinear,contactUs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(!isMyServiceRunning(ApplicationService.class)) {
            startService(new Intent(getApplicationContext(), ApplicationService.class));
        }
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_home);
        scannerLinear = (LinearLayout) findViewById(R.id.scannerLinear);
        connexionLinear=(LinearLayout) findViewById(R.id.connexionLinear);
        contactUs=(LinearLayout) findViewById(R.id.contactus);
        scannerLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getApplicationContext(), CaptureActivity.class);
                startActivity(i);

            }
        });

        connexionLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    //Traitement connectivité
            }
        });






    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        System.out.println(id);
        switch (id) {
            case R.id.settings:{
                Intent i=new Intent(getApplicationContext(),CaptureActivity.class);startActivity(i); finish();
                break;}
            case R.id.logout:{
              logout();break;
            }
            case R.id.languagesetting:{
                showLanguageDialog();break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void logout(){
    databaseLogin=getSharedPreferences("login", getBaseContext().MODE_PRIVATE);
    databaseLogineditor=databaseLogin.edit();
    databaseLogineditor.putBoolean("login",false);
    if(!databaseLogin.getBoolean("remember",false))
        databaseLogineditor.remove("email");

    databaseLogineditor.remove("password");
    databaseLogineditor.commit();
    Intent i=new Intent(getApplicationContext(),LoginActivity.class);startActivity(i); finish();

                }
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    private void showLanguageDialog(){
        final String[] lisitems={"French","Arabic","English"};
        AlertDialog.Builder mBuilder=new AlertDialog.Builder(Home.this);
        mBuilder.setTitle("Choose Language...");
        mBuilder.setSingleChoiceItems(lisitems, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(i==0){
                    setLocale("fr");
                    recreate();
                }
                else if(i==1){
                    setLocale("ar");
                    recreate();
                }
                else if(i==2){
                    setLocale("en");
                    recreate();
                }
                dialogInterface.dismiss();
            }
        });
        AlertDialog mDialog=mBuilder.create();
        mDialog.show();


    }

    private void setLocale(String lang){
        System.err.println(lang );
        Locale locale=new Locale(lang);
        Locale.setDefault(locale);
        Configuration configuration=new Configuration();
        configuration.locale=locale;
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
        editor.putString("My_Lang",lang);
        editor.apply();
    }


    public void loadLocale(){
        SharedPreferences prefs=getSharedPreferences("Settings",Activity.MODE_PRIVATE);
        String language=prefs.getString("My_Lang","");
        setLocale(language);
    }


}
