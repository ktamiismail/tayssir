package com.mercuriete.mrz.myapplication.reader;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.mercuriete.mrz.myapplication.LoginActivity;
import com.mercuriete.mrz.myapplication.R;

public class ApplicationService extends Service {
    Handler handler;
    private static final String CHANNEL_ID = "1250012";
    private static final String TAG =ApplicationService.class.getSimpleName();
    private Socket socket;
     SharedPreferences loginshared;
     SharedPreferences.Editor loginsharededitor;
    private User user;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        GetSocket app = new GetSocket();
        socket = app.getSocket();
        socket.connect();
      //  Identification();
        socket.on("NotifyToscan",handleIncomingNotifyToscan);
        socket.on("DoIdentification",handleIncomingDoIdentification);
        Identification();
        return super.onStartCommand(intent, flags, startId);
    }

    public void Identification() {

        JSONObject sendText = new JSONObject();


        if(loginshared.getBoolean("login",false)) {
           try {
               sendText.put("email", loginshared.getString("email",""));
               sendText.put("password", loginshared.getString("password",""));
               socket.emit("setIdentification", sendText);
           } catch (JSONException e) {
           }
       }
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler();
        loginshared= getSharedPreferences("login",getApplicationContext().MODE_PRIVATE);
        loginsharededitor=loginshared.edit();

        Log.i("info user", "onCreate: "+loginshared.getString("email","email")+loginshared.getString("password","password"));

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
       // super.onTaskRemoved(rootIntent);
        //  stopSelf();
    }


    public void MessageNotification(String message,String email){

        Intent intent;
        if(loginshared.getBoolean("login",false))
            intent = new Intent(this,CaptureActivity.class);
        else
            intent = new Intent(this, LoginActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder = new NotificationCompat.Builder(this,CHANNEL_ID);
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,TAG, NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableVibration(true);
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(notificationChannel);
        }
        else {
            notificationBuilder =  new NotificationCompat.Builder(this);
        }
        notificationBuilder
                .setContentTitle(email)
                .setContentText(message)
                        .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent)
                .setColor(Color.RED)
                .setSmallIcon(R.drawable.mdpi);


        notificationBuilder.setLights(Color.BLUE, 1000, 300);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,TAG
                    ,
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);

            Notification notification = new Notification.Builder(getApplicationContext(),CHANNEL_ID).build();
            startForeground(1, notification);
        }
        else {

            // startForeground(1, notification);
        }

    }

    private Emitter.Listener handleIncomingNotifyToscan = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    try {
                        String message=data.getString("message").toString();
                        String email=data.getString("email").toString();
                        MessageNotification(message,email);

                    } catch (JSONException e) {
                    }


                  //  System.err.println("cknjdckmcmmckmc");
                }
            });
        }
    };
    private Emitter.Listener handleIncomingDoIdentification = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                  Identification();
                }
            });
        }
    };
    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }
}
