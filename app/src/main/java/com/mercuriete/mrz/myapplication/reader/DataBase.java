package com.mercuriete.mrz.myapplication.reader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DataBase extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "MyMessages.db";
    private static final String TABLE_USER = "user";

    private static final String key_id = "id";
    private static final String key_email = "email";
    private static final String key_password = "password";

    public DataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {



        String createTableUser = "CREATE TABLE `user` (\n" +
                "\t`id`\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`email`\tTEXT,\n" +
                "\t`password`\tTEXT" +
                ");";
        db.execSQL(createTableUser);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS user ");
        onCreate(db);
    }


    public Boolean insertUser(String a, String b) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("email", a);
            values.put("password", b);
            db.insert("user", null, values);
            db.close();
            return true;
        } catch (SQLException e) {
            return false;
        }

    }


    public User getUser() {
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery("select * from user",null);
            if (cursor.moveToFirst()){
                db.close();
                return new User(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2));
                }
            else{
                return null;
            }

        } catch (SQLException e) {
            return null;
        }
    }



}

