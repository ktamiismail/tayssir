package com.mercuriete.mrz.myapplication.reader;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.mercuriete.mrz.myapplication.R;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends Activity implements View.OnClickListener {
    public static final int PERMISSION_ALL = 10;

    public static String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
    };

    private SharedPreferences loginPref;
    private SharedPreferences.Editor loginEditor;
    public SharedPreferences permissionsPref;
    public SharedPreferences.Editor permissionsEditor;

    EditText user;
    Socket socket;
    TextView res;
    EditText pass;
    private static final int uniqueId=45612;
    DataBase d;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        d=new DataBase(this);
      if(d.getUser()!=null){
            System.err.println("user not null");
            Intent intent = new Intent(getApplicationContext(), CaptureActivity.class);
            startActivity(intent);
            finish();
        }

        super.onCreate(savedInstanceState);
        d=new DataBase(this);
        setContentView(R.layout.activity_login);
        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        else
            Toast.makeText(this, "please check for permissions in settings->apps->" + getResources().getString(R.string.app_name), Toast.LENGTH_SHORT).show();



        GetSocket app = new GetSocket();
        socket = app.getSocket();
        socket.connect();
        socket.on("Authentification",handleIncomingAuthentifica);
        socket.on("nonAutho",handleIncomingAuthentificaNON);


        findViewById(R.id.connecter).setOnClickListener(this);
        user=findViewById(R.id.email);
        res=findViewById(R.id.resultat);
        pass=findViewById(R.id.password);


        loginPref = getApplicationContext().getSharedPreferences("login", MODE_PRIVATE);
        permissionsPref = getApplicationContext().getSharedPreferences("permissions", MODE_PRIVATE);
        loginEditor = loginPref.edit();
        permissionsEditor = permissionsPref.edit();

        String loginUserMailPref = loginPref.getString("loginUserMail", "");
        String loginPasswordPref = loginPref.getString("loginPassword", "");
        String loginUserNamePref = loginPref.getString("loginUserName", "");

        if(!"".equals(loginUserMailPref) && !"".equals(loginPasswordPref)){
            Intent intent=new Intent(getApplicationContext(),CaptureActivity.class);
            intent.putExtra("username", loginUserNamePref);

            JSONObject sendText = new JSONObject();
            try{
                sendText.put("email", loginUserMailPref);
                sendText.put("password", loginPasswordPref);

                socket.emit("Authentifier", sendText);
            }catch(JSONException e){
            }

            startActivity(intent);
            finish();
        }
    }



    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_ALL){
            if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                //Toast.makeText(this, "Permission to Storage is granted", Toast.LENGTH_SHORT).show();
                permissionsEditor.putBoolean("storagePermission", true);
            }
            else{
                Toast.makeText(this, "Application will not have image importation", Toast.LENGTH_SHORT).show();
                permissionsEditor.putBoolean("storagePermission", false);
                //finish();
            }
            if (grantResults.length > 2 && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                //Toast.makeText(getApplicationContext(), "Permission to record is granted", Toast.LENGTH_SHORT).show();
                permissionsEditor.putBoolean("micPermission", true);
            }
            else{
                Toast.makeText(this, "Application will not have audio on record", Toast.LENGTH_SHORT).show();
                permissionsEditor.putBoolean("micPermission", false);
                //finish();
            }
            permissionsEditor.apply();
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.connecter){
            Log.e("connect","heyhey");
            String username=user.getText().toString();
            JSONObject sendText = new JSONObject();
            try{
                sendText.put("email",username);
                sendText.put("password",pass.getText().toString());
                socket.emit("Authentifier", sendText);

            }catch(JSONException e){
            }
        }
    }




    private Emitter.Listener handleIncomingAuthentifica = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(insertUser()) {
                        Intent intent = new Intent(getApplicationContext(), CaptureActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Error base de donnees",Toast.LENGTH_LONG);

                    }
                }
            });
        }
    };

public boolean insertUser(){
    try {
        d = new DataBase(this);
        d.insertUser(user.getText().toString(), pass.getText().toString());
        return true;

    }
    catch (SQLException s){
        return false;
    }
    finally {
        d.close();
    }
    }


    private Emitter.Listener handleIncomingAuthentificaNON = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    JSONObject data = (JSONObject) args[0];
                    String reponse;
                    try {
                        String a=data.getString("msg").toString();
                        res.setText("Acces "+a);

                    } catch (JSONException e) {

                    }

                }
            });
        }
    };










}
