package com.mercuriete.mrz.myapplication.reader;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class GetSocket {

    private static Socket mSocket;
    {
        try {
            //mSocket = IO.socket("http://192.168.43.100:5000");
            mSocket = IO.socket("http://192.168.1.116:8887");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }
}
